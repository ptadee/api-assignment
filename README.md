# README #

### How do I get set up ###
* java version 1.8.0.121
* Apache Maven 3.5.0
* MySql 5.7.18

### Start API ###

* if run first time
-- mvn install
* other
-- mvn spring-boot:run -Dspring.profiles.active=local
