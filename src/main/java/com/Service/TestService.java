package com.Service;

import com.Dao.Bean.TaskBean;

import java.util.List;

/**
 * Created by b2q on 8/7/17.
 */
public interface TestService {
    List<TaskBean> listTask();
    TaskBean viewTask(Integer id);
    boolean addTask(TaskBean taskBean);
    boolean editTask(TaskBean taskBean);
    boolean setStatusTask(TaskBean taskBean);
    boolean deleteTask(TaskBean taskBean);
}
