package com.Service;

import com.Dao.Bean.TaskBean;
import com.Dao.TestDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by b2q on 8/7/17.
 */
@Service
public class TestServiceImpl implements TestService{

    @Autowired
    TestDao testDao;

    @Override
    public List<TaskBean> listTask() {
        return testDao.listTask();
    }

    @Override
    public TaskBean viewTask(Integer id) {
        return testDao.viewTask(id);
    }

    @Override
    public boolean addTask(TaskBean taskBean) {
        return testDao.addTask(taskBean);
    }

    @Override
    public boolean editTask(TaskBean taskBean) {
        return testDao.editTask(taskBean);
    }

    @Override
    public boolean setStatusTask(TaskBean taskBean) {
        return testDao.setStatusTask(taskBean);
    }

    @Override
    public boolean deleteTask(TaskBean taskBean) {
        return testDao.deleteTask(taskBean);
    }
}
