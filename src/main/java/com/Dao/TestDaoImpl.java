package com.Dao;

import com.Dao.Bean.TaskBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by b2q on 8/7/17.
 */
@Repository
public class TestDaoImpl implements  TestDao{

    @Autowired
    @Qualifier("jdbc")
    private JdbcTemplate jdbc;

    @Value("${status.done}")
    private String done;
    @Value("${status.pending}")
    private String pending;
    @Value("${status.delete}")
    private String delete;

    @Override
    public List<TaskBean> listTask() {
        return jdbc.query("SELECT id,name,status FROM task", new RowMapper<TaskBean>() {
            @Override
            public TaskBean mapRow(ResultSet resultSet, int i) throws SQLException {
                TaskBean allCul = new TaskBean();
                allCul.setId(resultSet.getInt("id"));
                allCul.setName(resultSet.getString("name"));
                allCul.setStatus(resultSet.getString("status"));
                return allCul;
            }
        });
    }

    @Override
    public TaskBean viewTask(Integer id) {
        return jdbc.queryForObject("SELECT id,name,status FROM task where id = ?",new Object[]{id}, new RowMapper<TaskBean>() {
            @Override
            public TaskBean mapRow(ResultSet resultSet, int i) throws SQLException {
                TaskBean allCul = new TaskBean();
                allCul.setId(resultSet.getInt("id"));
                allCul.setName(resultSet.getString("name"));
                allCul.setStatus(resultSet.getString("status"));
                return allCul;
            }
        });
    }

    @Override
    public boolean addTask(TaskBean taskBean) {
        return jdbc.update("INSERT INTO task(name,status) values(?,?)",new Object[]{taskBean.getName(),pending})!=0;
    }

    @Override
    public boolean editTask(TaskBean taskBean) {
        return jdbc.update("UPDATE task set name=? where id=?",new Object[]{taskBean.getName(),taskBean.getId()})!=0;
    }

    @Override
    public boolean setStatusTask(TaskBean taskBean) {
        return jdbc.update("UPDATE task set status=? where id=?",new Object[]{taskBean.getStatus(),taskBean.getId()})!=0;
    }

    @Override
    public boolean deleteTask(TaskBean taskBean) {
        return jdbc.update("UPDATE task set status=? where id=?",new Object[]{delete,taskBean.getId()})!=0;
    }
}
