package com.Controller;

import com.Controller.bean.JsonResponse;
import com.Dao.Bean.TaskBean;
import com.Service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by b2q on 8/3/17.
 */
@RestController
public class TestController {

    @Autowired
    TestService testService;

    @RequestMapping(value = "/listTask", method = RequestMethod.GET)
    JsonResponse listTask(){
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus("Failed");
        try {
            List<TaskBean> list = testService.listTask();


            jsonResponse.setResult(list);
            jsonResponse.setStatus("Success");


        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResponse;
    }
    @RequestMapping(value = "/viewTask", method = RequestMethod.GET)
    JsonResponse viewTask(@RequestParam Integer id){
        JsonResponse jsonResponse = new JsonResponse();
        jsonResponse.setStatus("Failed");
        try {
            TaskBean view = testService.viewTask(id);


            jsonResponse.setResult(view);
            jsonResponse.setStatus("Success");


        }catch (EmptyResultDataAccessException e){
            jsonResponse.setResult("");
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return jsonResponse;
    }
    @RequestMapping(value = "/addTask", method = RequestMethod.POST)
    JsonResponse addTask(@RequestBody TaskBean taskBean){
        JsonResponse jsonResponse = new JsonResponse();
        try {
            boolean add = testService.addTask(taskBean);
            jsonResponse.setResult(add);


        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResponse;
    }
    @RequestMapping(value = "/deleteTask", method = RequestMethod.PUT)
    JsonResponse deleteTask(@RequestBody TaskBean taskBean){
        JsonResponse jsonResponse = new JsonResponse();
        try {
            boolean deleteTask = testService.deleteTask(taskBean);
            jsonResponse.setResult(deleteTask);


        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResponse;
    }
    @RequestMapping(value = "/setStatusTask", method = RequestMethod.PUT)
    JsonResponse setStatusTask(@RequestBody TaskBean taskBean){
        JsonResponse jsonResponse = new JsonResponse();
        try {
            boolean setStatusTask = testService.setStatusTask(taskBean);
            jsonResponse.setResult(setStatusTask);


        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResponse;
    }
    @RequestMapping(value = "/editTask", method = RequestMethod.PUT)
    JsonResponse editTask(@RequestBody TaskBean taskBean){
        JsonResponse jsonResponse = new JsonResponse();
        try {
            boolean editTask = testService.editTask(taskBean);
            jsonResponse.setResult(editTask);

        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonResponse;
    }
}
